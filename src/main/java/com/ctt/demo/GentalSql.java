package com.ctt.demo;

import java.util.List;

public class GentalSql {

    private static String topLevel = "1";  //默认顶级ID


    /**
    表结构   mysql



     CREATE TABLE `gov_xzqh` (
     `id` varchar(64) NOT NULL COMMENT '主键ID',
     `tj_code` varchar(64) DEFAULT NULL COMMENT '统计ID 如是省份ID 后自动补充0  ',
     `name` varchar(64) DEFAULT NULL COMMENT '名称',
     `level` varchar(2) DEFAULT NULL COMMENT '1.省份 2.市 3区 4街道 5社区',
     `has_child` varchar(2) DEFAULT NULL COMMENT '是否有下一级  1表示有  0表示无',
     `parent_id` varchar(64) DEFAULT NULL COMMENT '上级ID 省份ID 默认为1',
     `code_path` varchar(100) DEFAULT NULL COMMENT 'Code路径 带1 '
     PRIMARY KEY (`id`)
     ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='2019国家5级行政区划';

     */


    public static void writeFileSql(String dataFile,String sqlFile){

        List<Node> dataList  = App2.getFileDataList(dataFile);
        for(Node n:dataList){
            TxtUtil.writeTxt1(sqlFile,getNodeSql(n));
        }
    }


    private static String getNodeSql(Node n){
        String sql = "INSERT INTO `gov_xzqh` VALUES (" +
                "'"+n.getCode()+"',"+
                "'"+n.getTjCode()+"',"+
                "'"+n.getName()+"',"+
                "'"+n.getLevel()+"',"+
                "'"+(n.isHasChild()?"1":"0")+"',"+
                "'"+getParentId(n.getCode(),n.getLevel())+"',"+
                "'"+getCodePath(n.getCode(),n.getLevel())+"'"+
                ");";

        return sql;
    }

    private static int[] levelLength = {2,2,2,3,3};
    private static int[] codeLength = {2,4,6,9,12};
    private static String getParentId(String code,int level){
        if(level == 1){
            return topLevel;
        }
        return code.substring(0,(code.length()-levelLength[level-1]));
    }

    private static String getCodePath(String code,int level){
        StringBuffer sb = new StringBuffer();

        if(level==5){
            sb.append("/"+code.substring(0,codeLength[level-1]));
            level--;
        }
        if(level==4){
            sb.insert(0,"/"+code.substring(0,codeLength[level-1]));
            level--;
        }
        if(level==3){
            sb.insert(0,"/"+code.substring(0,codeLength[level-1]));
            level--;
        }
        if(level==2){
            sb.insert(0,"/"+code.substring(0,codeLength[level-1]));
            level--;
        }
        if(level==1){
            sb.insert(0,"/"+code.substring(0,codeLength[level-1]));
        }
        sb.insert(0,topLevel);
        return sb.toString();
    }
}
