package com.ctt.demo;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @QQ  286400643
 * @author Ctt
 * @Date 2020-07-30
 */
public class App2
{
    //统计局官方超链接    根据实际情况修改
    private static String prefix = "http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/";
    //数据年份   根据需要获取的年份修改
    private static String year = "2021";

    private static String  suffix  = ".html";

    //@TODO 设定数据执行数据文件存储文件夹       根据电脑执行保存目录修改 我这边电脑为MAC
    public  static String localFilePrefix = "/Users/ctt/Desktop/";


    // windows 电脑
//    public  static String localFilePrefix = "D:\\";



    // 设定基础 省份代码不变。
    private static String[] provinceNames = {"北京市","天津市","河北省","山西省","内蒙古自治区","辽宁省","吉林省","黑龙江省",
            "上海市","江苏省","浙江省","安徽省","福建省","江西省","山东省","河南省",
            "湖北省","湖南省","广东省","广西壮族自治区","海南省","重庆市","四川省","贵州省",
            "云南省","西藏自治区","陕西省","甘肃省","青海省","宁夏回族自治区","新疆维吾尔自治区"};

    private static String[] pprvinceCodes = {"11","12","13","14","15","21","22","23",
            "31","32","33","34","35","36","37","41",
            "42","43","44","45","46","50","51","52",
            "53","54","61","62","63","64","65"};

    private static List<Node> proviceList = new ArrayList<>();;

    //本类使用于拔取国家统计局省份代码数据 5级
    //根据设定省2位 市2位  区2位 街道3位  居委会等 3位    例   11/01/01/001/001



    public static void main( String[] args )
    {
        System.out.println("执行期间触发超时错误为正常现象，请注意查看控制台执行结果。关注执行结束的文件");
        //省份文件地址
        String provinceFilePath =  localFilePrefix+"province.txt";
        //市文件地址
        String cityFilePath =  localFilePrefix+"city.txt";
        //区文件地址
        String countyFilePath =  localFilePrefix+"county.txt";
        //街道文件地址
        String townFilePath =  localFilePrefix+"town.txt";
        //社区文件地址
        String illageFilePath =  localFilePrefix+"village.txt";

        String sqlFile = localFilePrefix+"gov_xzqh.sql";

        // @TODO 一下方法 请根据执行步骤进行执行 。 否则会发生错误

         // 加载省份数据
        addProvinceData(provinceFilePath);
        //根据省份获取城市数据
        getData(provinceFilePath,cityFilePath,2);

        //根据城市获取区数据
//        getData(cityFilePath,countyFilePath,3);

        //根据区获取街道   这个我大概执行了30多分钟
//        getData(countyFilePath,townFilePath,4);
//
//        //根据街道获取社区  这个要执行好几个小时  挂机吧  。。。。。。。。数量太大
//        getData(townFilePath,illageFilePath,5);


//        System.out.println("执行完毕请注意数据文件，注意与错误文件进行对比，查看是否修复下载数据正确!");

        //获取街道数据出现了问题  检查执行文件发现错误的数据未被加载进去 。特别要注意 。
//        getErrorPathData(localFilePrefix+"village-111755.txt",localFilePrefix+"village-2.txt",5,2);


        //生成sql文件  助理GentalSql类中的表结构
//        getSqlFIle( provinceFilePath, cityFilePath,countyFilePath, townFilePath,illageFilePath, sqlFile);

    }

    /**
     *
     * @param provinceFilePath   数据文件
     * @param cityFilePath
     * @param countyFilePath
     * @param townFilePath
     * @param illageFilePath
     * @param sqlFile   sql文件
     */
    private static void getSqlFIle(String provinceFilePath,String cityFilePath,
                                   String countyFilePath,String townFilePath,
                                   String illageFilePath,String sqlFile){
//        @TODO 生成sql文件   两个参数 一个数据文件 一个sql输出文件  写入文件为追加模式
//        输出省份sql
        GentalSql.writeFileSql(provinceFilePath,sqlFile);

        //输出市sql
        GentalSql.writeFileSql(cityFilePath,sqlFile);

        //输出区sql
        GentalSql.writeFileSql(countyFilePath,sqlFile);

        //输出街道sql
        GentalSql.writeFileSql(townFilePath,sqlFile);
//
//        //输出社区sql
        GentalSql.writeFileSql(illageFilePath,sqlFile);
    }


    //写入省份数据
    public static void addProvinceData(String provinceFilePath){
        for(int i=0;i<provinceNames.length;i++){
            Node   pNode = new Node();
            String code = pprvinceCodes[i];
            pNode.setCode(code);
            pNode.setCodeStr(code);
            pNode.setSort(new Integer(code));
            pNode.setName(provinceNames[i]);
            pNode.setLevel(1);
            pNode.setpCode("0");
            pNode.setHasChild(true);
            pNode.setTjCode(code);
            pNode.setTempCode(getLevelPath(code+"0000000000",1)+code);
            TxtUtil.writeTxt1(provinceFilePath,pNode.getSaveTxt());
        }
    }



    //根据根存在数据获取区数据
    private static void getData(String parentDataFilePath,String dataPath,int level){
        String ErrorFilePath =  getNewFileErrorPath(dataPath);
        List<Node> resultList =  getFileDataList(parentDataFilePath);
        getDataByExitDataFile(resultList,dataPath,ErrorFilePath,level);
    }



    public static String GetUrl(String url){
        String result="";//访问返回结果
        BufferedReader read=null;//读取访问结果

        try {
            //创建url
            URL realurl=new URL(url);
            //打开连接
            URLConnection connection=realurl.openConnection();

            connection.setConnectTimeout(20000);
            connection.setReadTimeout(20000);

            // 设置通用的请求属性
//            connection.setRequestProperty("accept", "*/*");
//            connection.setRequestProperty("connection", "Keep-Alive");
//            connection.setRequestProperty("user-agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            //建立连接
            connection.connect();
            // 获取所有响应头字段
            // 定义 BufferedReader输入流来读取URL的响应
            read = new BufferedReader(new InputStreamReader(
                    connection.getInputStream(),
//                    "gbk"
                    "UTF-8"
            ));
            String line;//循环读取
            while ((line = read.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
//            e.printStackTrace();
        }finally{
            if(read!=null){//关闭流
                try {
                    read.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    //html元素格式整理
    //市级别   table class="citytable"   <tbody  <tr class ="citytr" <td   <a
    //区级别   table class="countytable" <tbody <tr calss = "countytr" <td <a
    //街道     towntable     towntr
    //          villagetable    villagetr   <tr  <td
    private static String[] className = {"citytr","countytr","towntr","villagetr"};
    private static int[] levelLength = {2,2,3,3};
    private static int[] codeLength = {4,6,9,12};
    public static List<Node> readHTML(String html,int level) {
        //解析htmlurl路径
        //URL u = new URL(url);
        //Document doc = Jsoup.parse(u, 0);
        List<Node> nodeList = new ArrayList<Node>();
        //解析html字符串
        Document doc = Jsoup.parse(html);
        //下面的代码基本与js相同
        Elements ems = doc.getElementsByClass(className[level-2]);


        for(Element e:ems){
            Node node = new Node();
            Elements f = e.getElementsByAttribute("href");
            if(f.size()>0) {
                String tjCode = f.get(0).text();
                node.setTjCode(tjCode); //设定统计code
                String code = tjCode.substring(0,codeLength[level-2]); //经过截取处理后的code 不包含后部0
                node.setCode(code);

                String shortCode = code.substring((code.length()-levelLength[level-2]),code.length());  //当前层级编号可能出现段号问题所以只能通过截取获得  不包含前后缀
                //node.setCodeStr(tempCode+"/"+shortCode);

                node.setName(f.get(1).text());
                node.setHasChild(true);
                node.setLevel(level);
                node.setSort(new Integer(shortCode));
                node.setTempCode(getLevelPath(tjCode,level)+code);
            //            System.out.println(f.html());
            }else{
                Elements tds = e.getElementsByTag("td");
                String tjCode = tds.get(0).text();
                node.setTjCode(tjCode); //设定统计code
                String code = tjCode.substring(0,codeLength[level-2]); //经过截取处理后的code 不包含后部0
                node.setCode(code);

                String shortCode = code.substring((code.length()-levelLength[level-2]),code.length());  //当前层级编号可能出现段号问题所以只能通过截取获得  不包含前后缀

                node.setName(level==5?tds.get(2).text():tds.get(1).text());
                node.setHasChild(false);
                node.setLevel(level);
                node.setTempCode(getLevelPath(tjCode,level)+code);
            }
            nodeList.add(node);
        }
        return nodeList;
    }

    private static void fzhrefNode(Element e,Node node){

    }


    //返回补0的函数
    private static String get0Code(int length,int num){
        return String.format("%0"+length+"d",num);
    }




    private static int[] levelSubLength = {0,2,4,6,9};
    public static String getLevelPath(String tjCode,int level){
        String aPaht = a(tjCode,levelSubLength[level-1]);
        return aPaht;
    }

    //根据level获取访问前缀
    private static String a(String tjCode,int len){
        String path = "/";
        if(len>=2){
            path+=tjCode.substring(0,2)+"/";
        }
        if(len>=4){
            path +=tjCode.substring(2,4)+"/";
        }
        if(len>=6){
            path +=tjCode.substring(4,6)+"/";
        }
        if(len>=9){
            path +=tjCode.substring(6,9)+"/";
        }
        if(len==12){
            path +=tjCode.substring(9,12)+"/";
        }
        return path;
    }

    //解析txt数据
    public static Node jiexiTxt(String code){
        String[] sz = code.split(",");
        Node n = new Node();
        n.setCode(sz[0]);
        n.setTjCode(sz[1]);
        n.setName(sz[2]);
        n.setLevel(new Integer(sz[3]));
        n.setHasChild("true".equals(sz[4])?true:false);
        n.setTempCode(sz[5]);
        return n;
    }


    /**
     * 第一次获取数据发现错误 再次进行错误数据的列表获取
     * @param errorFilePath   错误文件地址
     * @param dataFilePath   数据文件地址
     * @Param level  等级   省1  市2  区3 街道4 社区5
     * @param count 修复次数错误  第一次传如1  自动递增修复
     */
    public static void getErrorPathData(String errorFilePath,String dataFilePath,int level,int count){
        System.out.println("第"+count+"次修复开始： "+"时间"+System.currentTimeMillis()+"开始修复错误列表:"+errorFilePath);
        List<String>  errorPathList = new ArrayList<String>();//获取旧的错误列表文件
        File file = new File(errorFilePath);
        if(file.isFile() && file.exists()){
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                StringBuffer sb = new StringBuffer();
                String text = null;
                while((text = bufferedReader.readLine()) != null){
                    if(!"".equals(text)){
                        errorPathList.add(text);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        String errorNewPath = "";
        //重新触发数据
        boolean hasError = false;
        for(String url:errorPathList){
            String sendGET = GetUrl(url);
            //如果获取为空 加入错误列表
            if("".equals(sendGET)){
                hasError= true;
                if("".equals(errorNewPath)){errorNewPath=getNewFileErrorPath(errorFilePath);
                System.out.println("第"+count+"次修复情况： "+"重复获取数据发生错误，执行完毕后将结束修复，重复执行getErrorPathData方法 请关注错误文件："+errorNewPath);}
                TxtUtil.writeTxt1(errorNewPath,url);
                continue;
            }
            List<Node> result = readHTML(sendGET,level);
            for(Node pNode:result){
                TxtUtil.writeTxt1(dataFilePath,pNode.getSaveTxt());
            }
        }
        System.out.println("第"+count+"次修复完毕： "+"时间"+System.currentTimeMillis()+"修复执行完毕，请关注是否发生修复错误。");

        if(hasError){
            int newCount = count+1;
            getErrorPathData(errorNewPath,dataFilePath,level,newCount);
        }

    }

    //生成二次错误文件
    public static String getNewFileErrorPath(String errorPath){
        Date  dt = new Date();
        String filep = dt.getHours()+""+dt.getMinutes()+""+dt.getSeconds();
        return errorPath.replace(".txt","-"+filep+".txt");
    }

    /**
     * 根据文件获取已获取的数据列表，用于二次查询
     * @param filePath
     * @return
     */
    public static List<Node> getFileDataList(String filePath){
        List<Node> dataList = new ArrayList<Node>();
        File file = new File(filePath);
        if(file.isFile() && file.exists()){
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                StringBuffer sb = new StringBuffer();
                String text = null;
                while((text = bufferedReader.readLine()) != null){
                    if(!"".equals(text)){
                        dataList.add(jiexiTxt(text));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return dataList;
    }

    public static void getDataByExitDataFile(List<Node> exitDataList,String dataFilePath,String errorFilePath,int level){
        boolean hasError = false;
        for(Node n:exitDataList){
            if(!n.isHasChild()){continue;}
            String url = prefix +year+n.getTempCode()+suffix;
            String sendGET = GetUrl(url);
            //如果获取为空 加入错误列表
            if("".equals(sendGET)){TxtUtil.writeTxt1(errorFilePath,url);hasError = true;continue;}

            List<Node> result = readHTML(sendGET,level);
            for(Node pNode:result){
                TxtUtil.writeTxt1(dataFilePath,pNode.getSaveTxt());
            }
        }

        //执行再次触发错误数据获取，如发生错误将生成三次错误文件
        if(hasError){
            System.out.println("数据获取发生错误，请关注错误文件："+errorFilePath);
            System.out.println("获取数据发生错误，开始重新加载数据错误列表:"+errorFilePath);
            getErrorPathData(errorFilePath,dataFilePath,level,1);
        }
        System.out.println("过程执行完毕"+System.currentTimeMillis());
    }

}
