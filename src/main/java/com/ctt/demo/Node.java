package com.ctt.demo;

public class Node {

    //id
    private String code;
    //id集合
    private String codeStr;
    //统计代码
    private String tjCode;
    //名称
    private String name;
    //父ID
    private String pCode;
    //是否有子集
    private boolean hasChild;
    //级别  省为1级 依次类推
    private int level;
    //当前级别况下排序
    private int sort;

    //用于后续的请求访问
    private String tempCode;

    public String getTempCode() {
        return tempCode;
    }

    public void setTempCode(String tempCode) {
        this.tempCode = tempCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeStr() {
        return codeStr;
    }

    public void setCodeStr(String codeStr) {
        this.codeStr = codeStr;
    }

    public String getTjCode() {
        return tjCode;
    }

    public void setTjCode(String tjCode) {
        this.tjCode = tjCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public boolean isHasChild() {
        return hasChild;
    }

    public void setHasChild(boolean hasChild) {
        this.hasChild = hasChild;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }


    public String getSaveTxt(){
        return this.code+","+this.tjCode+","+this.name+","+this.level+","+this.hasChild+","+this.tempCode;
    }
}
